using System;
using UnityEngine;

[Serializable]
public class RunningStateConfig
{
    [SerializeField, Range(0, 10)] private float _moveSpeed;
    [SerializeField, Range(0, 10)] private float _walkSpeed;
    [SerializeField, Range(0, 10)] private float _runSpeed;

    public float MoveSpeed => _moveSpeed;
    public float WalkSpeed => _walkSpeed;
    public float RunSpeed => _runSpeed;
}
