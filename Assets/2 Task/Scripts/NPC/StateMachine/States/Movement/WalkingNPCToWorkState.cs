using UnityEngine;

public class WalkingNPCToWorkState : NPCMovementState
{
    private NpcMovementConfig _config;
    private float _timeToWalk;

    public WalkingNPCToWorkState(IStateSwitcherNPC stateSwitcherNPC, NPC npc) : base(stateSwitcherNPC, npc)
    {
        _config = npc.Config.MovementConfig;
    }

    public override void Enter()
    {
        base.Enter();
        _timeToWalk = _config.TimeToWalkToWork;
    }

    public override void Update()
    {
        base.Update();

        if ((_timeToWalk -= Time.deltaTime) <= 0)
        {
            StateSwitcherNPC.SwitchState<WorkingState>();
        }
    }
}
