using UnityEngine;
using System;

[Serializable]
public class NpcMovementConfig 
{
    [SerializeField, Range(0f, 15f)] private float _timeToWalkToRelax;
    [SerializeField, Range(0f, 15f)] private float _timeToWalkToWork;

    public float TimeToWalkToRelax => _timeToWalkToRelax;
    public float TimeToWalkToWork => _timeToWalkToWork;
}
