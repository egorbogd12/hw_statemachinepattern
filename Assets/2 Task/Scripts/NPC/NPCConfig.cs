using UnityEngine;

[CreateAssetMenu(fileName = "NPCConfig", menuName = "Configs/NpcConfig")]
public class NPCConfig : ScriptableObject
{
    [SerializeField] private NpcMovementConfig _movementConfig;
    [SerializeField] private NPCActionsConfig _actionsConfig;

    public NpcMovementConfig MovementConfig => _movementConfig;
    public NPCActionsConfig ActionsConfig => _actionsConfig;
}
