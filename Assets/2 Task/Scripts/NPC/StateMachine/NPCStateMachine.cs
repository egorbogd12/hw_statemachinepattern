using System.Collections.Generic;
using System.Linq;

public class NPCStateMachine : IStateSwitcherNPC
{
    private List<IStateNPC> _states;
    private IStateNPC _currentState;

    public NPCStateMachine(NPC npc)
    {
        _states = new List<IStateNPC>()
        {
            new RelaxState(this, npc),
            new WorkingState(this, npc),
            new WalkingNPCToRelaxState(this, npc),
            new WalkingNPCToWorkState(this, npc)
        };

        _currentState = _states[0];
        _currentState.Enter();
    }

    public void SwitchState<T>() where T : IStateNPC
    {
        IStateNPC state = _states.FirstOrDefault(state => state is T);

        _currentState.Exit();
        _currentState = state;
        _currentState.Enter();
    }

    public void Update() => _currentState.Update(); 
}
