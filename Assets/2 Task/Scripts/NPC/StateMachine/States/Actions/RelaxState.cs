using UnityEngine;

public class RelaxState : NPCActionsState
{
    private NPCActionsConfig _config;
    private float _timeToRelax;

    public RelaxState(IStateSwitcherNPC stateSwitcherNPC, NPC npc) : base(stateSwitcherNPC, npc)
    {
        _config = npc.Config.ActionsConfig;
    }
    public override void Enter()
    {
        base.Enter();
        _timeToRelax = _config.TimeToRelax;

    }

    public override void Update()
    {
        base.Update();

        if ((_timeToRelax -= Time.deltaTime) <= 0)
        {
            StateSwitcherNPC.SwitchState<WalkingNPCToWorkState>();
        }
    }
}
