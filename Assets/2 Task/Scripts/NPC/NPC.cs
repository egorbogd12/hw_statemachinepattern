using UnityEngine;

public class NPC : MonoBehaviour
{
    [SerializeField] private NPCConfig _config;

    public NPCConfig Config => _config;

    private NPCStateMachine _stateMachine;

    private void Awake()
    {
        _stateMachine = new NPCStateMachine(this);
    }

    private void Update()
    {
        _stateMachine.Update();
    }
}
