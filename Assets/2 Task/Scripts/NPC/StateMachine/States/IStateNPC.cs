public interface IStateNPC
{
    public void Enter();
    public void Exit();
    public void Update();
    
}
