using UnityEngine;
using UnityEngine.InputSystem;

public class GroundedState : MovementState
{
    private const float KeyDown = 1;
    private readonly GroundChecker _groundChecker;

    public GroundedState(IStateSwitcher stateSwitcher, Character character, StateMachineData data) : base(stateSwitcher, character, data)
        => _groundChecker = character.GroundChecker;

    //public bool IsShiftDown => Input.Movement.Run.ReadValue<float>() == KeyDown;
    //public bool IsCntrlDown => Input.Movement.Walk.IsPressed();

    public override void Enter()
    {
        base.Enter();

        View.StartGrounded();
    }

    public override void Exit()
    {
        base.Exit();

        View.StopGrounded();
    }

    public override void Update()
    {
        base.Update();

        if (_groundChecker.IsTouches == false)
            StateSwitcher.SwitchState<FallingState>();
        //if (IsShiftDown)
        //{
        //    StateSwitcher.SwitchState<FastRunningState>();
        //}
        if (IsCntrlDown)
        {
            Debug.Log("Ctrl �����: " + IsCntrlDown);
        }
    }

    protected override void AddInputActionsCallbacks()
    {
        base.AddInputActionsCallbacks();

        Input.Movement.Jump.started += OnJumpKeyPressed;

        Input.Movement.Walk.started += OnWalkKeyPressing;
        Input.Movement.Walk.canceled += OnWalkKeyCancelled;

        Input.Movement.Run.started += OnRunKeyPressing;
        Input.Movement.Run.canceled += OnRunKeyCancelled;

    }

    protected override void RemoveInputActionsCallbacks()
    {
        base.RemoveInputActionsCallbacks();

        Input.Movement.Jump.started -= OnJumpKeyPressed;

        Input.Movement.Walk.started -= OnWalkKeyPressing;
        Input.Movement.Walk.canceled -= OnWalkKeyCancelled;

        Input.Movement.Run.started -= OnRunKeyPressing;
        Input.Movement.Run.canceled -= OnRunKeyCancelled;


    }

    private void OnJumpKeyPressed(InputAction.CallbackContext obj) => StateSwitcher.SwitchState<JumpState>();

    private void OnWalkKeyPressing(InputAction.CallbackContext obj) => StateSwitcher.SwitchState<WalkingState>();

    private void OnWalkKeyCancelled(InputAction.CallbackContext obj) => StateSwitcher.SwitchState<RunningState>();

    private void OnRunKeyPressing(InputAction.CallbackContext obj) => StateSwitcher.SwitchState<FastRunningState>();

    private void OnRunKeyCancelled(InputAction.CallbackContext obj) => StateSwitcher.SwitchState<RunningState>();

}
