using UnityEngine;

public class WorkingState : NPCActionsState
{
    private NPCActionsConfig _config;
    private float _timeToWork;

    public WorkingState(IStateSwitcherNPC stateSwitcherNPC, NPC npc) : base(stateSwitcherNPC, npc)
    {
        _config = npc.Config.ActionsConfig;
    }

    public override void Enter()
    {
        base.Enter();
        _timeToWork = _config.TimeToWork;

    }

    public override void Update()
    {
        base.Update();

        if ((_timeToWork -= Time.deltaTime) <= 0)
        {
            StateSwitcherNPC.SwitchState<WalkingNPCToRelaxState>();
        }
    }
}
