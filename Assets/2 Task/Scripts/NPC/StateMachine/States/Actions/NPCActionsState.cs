using UnityEngine;

public class NPCActionsState : IStateNPC
{
    protected readonly IStateSwitcherNPC StateSwitcherNPC;

    public NPCActionsState(IStateSwitcherNPC stateSwitcherNPC, NPC npc)
    {
        StateSwitcherNPC = stateSwitcherNPC;
    }

    public virtual void Enter() 
    {
        Debug.Log(GetType());
    }

    public virtual void Exit() { }


    public virtual void Update() { }

}
