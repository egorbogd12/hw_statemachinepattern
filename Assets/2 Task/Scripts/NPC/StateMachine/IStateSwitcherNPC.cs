public interface IStateSwitcherNPC 
{
    public void SwitchState<T>() where T : IStateNPC;
}
