using UnityEngine;

public class WalkingNPCToRelaxState : NPCMovementState
{
    private NpcMovementConfig _config;
    private float _timeToWalk;

    public WalkingNPCToRelaxState(IStateSwitcherNPC stateSwitcherNPC, NPC npc) : base(stateSwitcherNPC, npc)
    {
        _config = npc.Config.MovementConfig;
    }
    public override void Enter()
    {
        base.Enter();
        _timeToWalk = _config.TimeToWalkToRelax;
    }

    public override void Update()
    {
        base.Update();

        if ((_timeToWalk -= Time.deltaTime) <= 0)
        {
            StateSwitcherNPC.SwitchState<RelaxState>();
        }
    }
}
