using UnityEngine;
using System;

[Serializable]
public class NPCActionsConfig
{
    [SerializeField, Range(0f,10f)] private float _timeToRelax;
    [SerializeField, Range(0f,10f)] private float _timeToWork;

    public float TimeToRelax => _timeToRelax;
    public float TimeToWork => _timeToWork;
}
